#!/bin/bash


# Get AWS EC2 inventory in a format suitable for ansible




# Source our secret credentials
# source ec2_ro_creds.sh 

# AWS region we're pulling from - would be nice to just have an "all" here but the CLI doesn't seem to allow it
region="us-east-1"



# Step 1: Get AWS complete inventory with tags we're interested in
# (only a single tag, called "Name" is pulled.  All EC2 VM's should have this Name tag, otherwise this script might break!)
EC2_ALL=$(aws --output text --region ${region} ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId,ImageId,Tags[?Key==`Name`].Value,PrivateIpAddress,PublicIpAddress,State.Name]')


# These are our host groups - we'll add to these strings and output them all to the file at the end
# Right now there are 4 possible groups - powered on, powered off, masters, and workers
ec2_off="[EC2_Powered_Off]"
ec2_on="[EC2_Powered_On]"
kube_master="[kube_master]"
kube_worker="[kube_worker]"


# This loop reads our EC2_ALL (output from the AWS CLI) line by line and processes each ec2 entry accordingly
while read -r line; do
	
	# For some reason, tags appear on a second line.  We need to read that one as well and join it with the "main" line for processing
	# (AWS CLI puts the tag on the second line of output per instance for some reason)
	read -r line2
	line="${line2} $line"
	


  # The line is now a space-separated bunch of attributes
  # This gets each attribute into an individual variable so we can work with it
	ec2_name=`echo $line | awk '{print $1}'`
	ami_id=`echo $line | awk '{print $2}'`
	ami_image=`echo $line | awk '{print $3}'`
	private_ip=`echo $line | awk '{print $4}'`
	public_ip=`echo $line | awk '{print $5}'`
	ec2_state=`echo $line | awk '{print $6}'`



  # Separate machines into "running" and "not running"
  # We add each one to the appropriate Ansible group
	if [ "${ec2_state}" != "running" ]; then
		ec2_off="${ec2_off}\n${public_ip} name=\"${ec2_name}\"  state=\"${ec2_state}\"  ansible_host=\"${public_ip}\"  private_ip=\"${private_ip}\"  instance_id=\"${ami_id}\"  ami_image=\"${ami_image}\""
	else
		ec2_on="${ec2_on}\n${public_ip} name=\"${ec2_name}\"  state=\"${ec2_state}\"  ansible_host=\"${public_ip}\"  private_ip=\"${private_ip}\"  instance_id=\"${ami_id}\"  ami_image=\"${ami_image}\""
	fi

  
  # Figure out whether each machine is a worker or a master, and group it accordingly
  if [[ $ec2_name == *"kube_master"* ]]; then
    kube_master="${kube_master}\n${public_ip} name=\"${ec2_name}\"  state=\"${ec2_state}\"  ansible_host=\"${public_ip}\"  private_ip=\"${private_ip}\"  instance_id=\"${ami_id}\"  ami_image=\"${ami_image}\""
  fi

  if [[ $ec2_name == *"kube_worker"* ]]; then
    kube_worker="${kube_worker}\n${public_ip} name=\"${ec2_name}\"  state=\"${ec2_state}\"  ansible_host=\"${public_ip}\"  private_ip=\"${private_ip}\"  instance_id=\"${ami_id}\"  ami_image=\"${ami_image}\""
  fi

	

done <<< "$EC2_ALL"


# Spit out all our group variables in a row to stdout
# This forms the entirety of our inventory, and can be redirected to a file, or wherever we want
echo -e "${ec2_on}\n\n${ec2_off}\n\n${kube_master}\n\n${kube_worker}"
